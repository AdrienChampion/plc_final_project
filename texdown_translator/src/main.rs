/*! Skeleton crate for texdown translation project.

The [texdown project](../lib_texdown/index.html) this crate depends on
features [generation of a texdown file from the AST](../src/lib_texdown/ast/ast_to_texdown.rs.html).
You can draw inspiration from it to generate HTML or LaTeX.

See also the [`to_file` function on `Frames`](../lib_texdown/ast/struct.Frames.html#method.to_file).

# How to keep your project synced with the group creating the AST

File `Cargo.toml` is the manifest file for cargo handling the dependencies,
among other things. As I'm writing these lines, the dependency on `lib_texdown`
looks like this:

```
lib_texdown = { git = "https://bitbucket.org/AdrienChampion/texdown.git", rev = "8b915ec" }
```

The `rev` field is the revision of the repo your project depends on. Here it
corresponds to a commit on the
[texdown repo](https://bitbucket.org/AdrienChampion/texdown).

You can see all the commits [here](https://bitbucket.org/AdrienChampion/texdown/commits/all). If you want to use the latest version, make sure you replace the
value of the `rev` with the latest (top-most) commit number.

If the latest commit is `983c88c`, then you should update `Cargo.toml` to

```
lib_texdown = { git = "https://bitbucket.org/AdrienChampion/texdown.git", rev = "983c88c" }
```

You probably want to **follow** the texdown repo. You can do so on
[this page](https://bitbucket.org/AdrienChampion/texdown/overview),
by clicking on the *eye* button that's at the top-right of the page.
That way you'll be modified as soon as a new commit is pushed to the repo.

*/

#![allow(non_upper_case_globals)]

extern crate clap ;
extern crate lib_texdown ;

use clap::{ Arg, App } ;

pub use lib_texdown::ast::{ Frames, RichTxt, SmplTxt } ;


/// CLAP key for output directory.
static output_key: & 'static str = "output_dir" ;
/// Default output directory.
static output_default: & 'static str = "output" ;
/// CLAP key for input file.
static input_key: & 'static str = "input_file" ;
/// CLAP key for verbosity.
static verbose_key: & 'static str = "VERB" ;

/// Extension for the files generated.
/// CHANGE THIS DEPENDING ON WHAT YOU GENERATE.
pub static extension: & 'static str = "ext" ;

/// Returns result if value is OK, prints the error and exits otherwise.
fn try<T>(res: Result<T, String>) -> T {
  match res {
    Ok(res) => res,
    Err(err) => {
      use std::process::exit ;
      for line in err.lines() {
        println!("> {}", line)
      } ;
      exit(2)
    }
  }
}

/// Returns result if value is OK, prints the error and exits otherwise.
///
/// IO version.
fn try_io<T>(res: std::io::Result<T>, txt: String) -> T {
  match res {
    Ok(res) => res,
    Err(err) => {
      use std::process::exit ;
      println!("{}", txt) ;
      println!("> {}", err) ;
      exit(2)
    }
  }
}

fn main() {
  use std::path::Path ;
  use std::fs::DirBuilder ;

  // Handling CLA (Command Line Arguments).
  let matches = {
    App::new("texdown to ...").version(
      "0.1.0"
    ).author(
      "Adrien Champion <adrien.champion@email.com>"
    ).about(
      "Translates texdown files to ..."
    ).arg(
      Arg::with_name(output_key).short("o").long("out").help(
        "Sets the output directory"
      ).takes_value(true)
    ).arg(
      Arg::with_name(input_key).help(
        "The input file"
      ).required(true).index(1)
    ).arg(
      Arg::with_name(verbose_key).short("v").help("Activates verbose output")
    ).get_matches()
  } ;

  // Retrieving output dir.
  let out_dir = matches.value_of(output_key).unwrap_or(output_default) ;

  // Retrieving input file.
  let source = {
    match matches.value_of(input_key) {
      Some(input) => input,
      None => unreachable!(),
    }
  } ;

  // Retrieving verbosity.
  let verbose = match matches.occurrences_of(verbose_key) {
    0 => false, _ => true,
  } ;

  if verbose {
    println!("")
  } ;

  // Retrieving frames.
  let frames = try(Frames::of_file(source, verbose)) ;

  // Create output directory if necessary.
  try_io(
    DirBuilder::new().recursive(true).create(& out_dir),
    format!("Error creating output directory \"{}\":", out_dir)
  ) ;

  // **Safely** building path to target file.
  {
    let source_path = Path::new(source) ;
    let mut target_path = Path::new(out_dir).to_path_buf() ;
    let file_name = match source_path.file_stem() {
      Some(file_name) => file_name,
      None => unreachable!(),
    } ;
    target_path.push(file_name) ;
    target_path.set_extension(extension) ;
    let target = target_path.as_path() ;

    let target = target.to_str().unwrap() ;

    if verbose {
      println!("|===| Translating to file \"{}\"", target) ;
    } ;

    // Write frames to target file.
    try_io(
      // Writing to texdown here.
      // CHANGE THIS.
      frames.to_texdown(target),
      format!("Error writing result file \"{}\":", target)
    ) ;

    if verbose {
      println!("| done") ;
      println!("|===|") ;
      println!("")
    }
  }

}