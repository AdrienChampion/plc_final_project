//! Skeleton crate for a 2048 AI.

extern crate lib_gol ;

use lib_gol::{ Conf, GridOps, RealGrid } ;

/// Displays a grid.
fn display_grid(_grid: & RealGrid) {
  panic!("display is unimplemented")
}

/// Entry point.
fn main() {
  // Configuration stores the size of the grid and the names.
  let conf = Conf::mk(
    42, 42, vec![ "Player 1".to_string(), "Player 2".to_string() ]
  ) ;

  // How you create the initial grid for all players is up to you.
  // At first you can rely on the parsers (homework 1) from `gol::frontend`.
  // See the doc for more info.

  // Empty grid for now.
  let mut grid = RealGrid::empty(conf) ;

  // Rendering loop.
  'rendering: loop {

    // Display grid.
    display_grid(& grid) ;

    // Take a step.
    match grid.update_seq() {
      // Update went fine.
      Ok(has_moved) => if ! has_moved {
        println!("done, should probably notify the players")
      },
      // Problem during update.
      Err(e) => for line in e.lines() {
        println!("error (should probably notify the players):") ;
        println!("| {}", line)
      },
    }

    // Probably want to wait a bit before looping to avoid causing brain trauma
    // to the users by flashing 100_000 frames per second. Would be a cool
    // project though.
  }
}